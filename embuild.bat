call D:\emsdk\emsdk_env.bat
mkdir html
pushd html
echo j|del *.*

::call emcc main.cpp --bind -s WASM=1 -s  -o ../public/wasm/main.js --preload-file assets --use-preload-plugins
call emcc ../src/wasm_xpbd/em_main.cpp -O3 -s WASM=1 -s -o ./index.html -s FETCH=1 -I ../src/extern/ -s MIN_WEBGL_VERSION=2 -s MAX_WEBGL_VERSION=2 -s ALLOW_MEMORY_GROWTH=1

popd

echo EM_MAIN DONE