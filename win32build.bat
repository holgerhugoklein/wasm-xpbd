@echo off

call "D:\Visual studio\VC\Auxiliary\Build\vcvars64.bat"

set commonCompilerFlags=/nologo /O2 /Zi /WX /wd4127 /wd4100 /wd4101 /wd4459 /wd4189 /EHsc

set commonLinkerFlags=/link /LIBPATH:"./libs/x64/" 

set libs= opengl32.lib D:\libs\SDL2.lib D:\libs\SDL2main.lib shell32.lib D:\libs\glew32.lib 
::vdvapi32.lib

set otherincludes=/I ..\src\extern\

mkdir build
pushd build
echo j|del *.*

xcopy ..\libs\x64\SDL2.dll .\

cl /DSLOW=1  %otherincludes% %commonCompilerFlags% ..\src\wasm_xpbd\win32_main.cpp  %commonLinkerFlags%  %libs% /SUBSYSTEM:CONSOLE

::devenv win32_main.exe
popd
echo win32 done


