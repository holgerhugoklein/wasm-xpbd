#include <stdio.h>

#include <emscripten/html5.h>
#include <emscripten.h>


#include "./xpbd/sim.h"
#include "./xpbd/grab_logic.h"


#include "render/em_opengl.h"

#include "input/em_input.h"

#include "./assets/assets.h"


f64 em_nowS() {
    return emscripten_performance_now() / 1000.;
}

f64 start;
f64 last_start;
f64 last_frame_durS;

RED::assets::Assets assets;
RED::sim::Sim sim;
RED::render::cam::Camera cam{};
RED::sim::grab::Grab grab;
RED::render::scene::Scene scene{};

b32 paused = true;
b32 rotate = true;

void em_mainLoop() {
    start = em_nowS();
    last_frame_durS = start - last_start;
    last_start = start;
    RED::input::Input input = RED::input::generateFromEM();

    RED::render::cam::move(&cam, RED::input::getMoveVec(input), last_frame_durS);
    RED::render::cam::rotate(&cam, input.rotCameraCmds.xy, last_frame_durS);

    RED::sim::grab::handleGrabCommands(input, &grab, &sim, &cam, &scene);        

    if(RED::input::em_input.keystates['+'] == RED::input::KEYSTATE::DOWN)
    {
        RED::render::make_canvas_bigger();
        //RED::input::em_input.keystates['+'] = RED::input::KEYSTATE::NONE;
    }
    if(RED::input::em_input.keystates['-'] == RED::input::KEYSTATE::DOWN)
    {
        RED::render::make_canvas_smaller();
        //RED::input::em_input.keystates['-'] = RED::input::KEYSTATE::NONE;
    }

    if(input.step) paused = !paused;

    if(input.toggle_grav) sim.grav = !sim.grav;

    if(input.toggle_rot) rotate =  !rotate;

    if(!paused || input.step_once)
    {
        if(rotate)
        {           
            RED::math::v3 wall_turn_point = {0, 6, 0};
            RED::math::v3 wall_turn_axis = {0, 0, 1};
            for(u32 i = 0; i < 6; ++i)
            {
                RED::sim::RigidBody *wall = sim.rigid_bodies + i;
                RED::math::m4 trans = glm::translate(RED::math::m4(1), -wall->pos);
                trans = glm::rotate(trans, (f32) last_frame_durS * (f32) M_PI * 2 * .1f, wall_turn_axis);
                wall->pos = RED::math::m3(trans) * wall->pos;
                wall->quat = glm::toQuat(RED::math::m3(trans)) * wall->quat;
            } 
        }
        RED::sim::simOneFrame(&sim, last_frame_durS, &scene);
    }

    RED::render::renderSceneAndSwapWindows(&scene, &cam);
}

int main() {

    RED::render::cam::setupCamera(&cam, glm::vec3(0., 0., 16.), glm::vec3(0, 0, -1), 0, 0, glm::radians(45.));
    RED::render::initOpenGL(&cam);
    
    RED::assets::init(&assets);
    RED::render::ogl3::triMeshAdded(&assets.cube_mesh);
    RED::render::ogl3::triMeshAdded(&assets.plane_mesh);    

    RED::sim::init(&sim, &grab);    

    RED::math::v3 nice_red(.4, .1, .1);
    RED::math::v3 walls(.2, .1, .2);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 0., 6), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(-1., 0., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0, -6, 0), 
        glm::angleAxis(glm::radians(0.f), RED::math::v3(0., 1., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 0., -6), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(1., 0., 0.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(6., 0., -0), 
        glm::angleAxis(glm::radians(90.f), RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(-6., 0, -0), 
        glm::angleAxis(glm::radians(90.f), -RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);

    RED::sim::addRigidBodyFromTriangleMesh(
        &sim, &assets.plane_mesh, RED::math::v3(0., 6, -0), 
        glm::angleAxis(glm::radians(180.f), -RED::math::v3(0., 0., 1.)), 
        0, RED::math::v3(6, 6, 6), walls);

    for(u32 i = 1; i < 51; ++i)    
    {
        float x = ((float)rand()/(float)(RAND_MAX)) * .5;
        float y = ((float)rand()/(float)(RAND_MAX)) * .5;
        float z = ((float)rand()/(float)(RAND_MAX)) * .5;
        RED::sim::RigidBody *rb = RED::sim::addRigidBodyFromTriangleMesh(
            &sim, &assets.cube_mesh, RED::math::v3(i * .01), 
            glm::angleAxis(glm::radians(150.f), RED::math::v3(1., 0., 0.)), 
            1.f, RED::math::v3(x + .3, y + .3, z + .3) , {x, y, z});
    }

    scene.point_light_1 = {0, 3, 6};
    
    for(u32 i = 1; i < sim.amt_rigid_bodies; ++i)
    {
        RED::sim::RigidBody *rb = sim.rigid_bodies + i;
        RED::render::scene::pushRigidBody(&scene, rb);
    }

    printf(
        R"man(
        ~~~~~CONTROLS~~~~~
        - + to make screen bigger
        - - to make screen smaller
        -WASD to move the camera
        -Hold right mouse and drag to rotate the camera
        -Hold left mouse button and drag to drag objects
        -Press q to toggle running the simulation
        -Press e to single step the simulation
        -Press g to toggle gravity
        -Press r to toggle rotating the walls
        )man");


    last_start = em_nowS() - (1. / 60.);

    RED::input::setup();

    emscripten_set_main_loop(em_mainLoop, 0, false);

    return 0;
}