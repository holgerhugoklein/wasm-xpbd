#pragma once

#include <typedefs.h>

#include "../../maths/maths.h"


namespace RED
{
    namespace sim
    {
        namespace id
        {
             typedef u32 PhysBodyId;

            //stores whether the body is soft or rigid in the first bit
            struct BodyIdData
            {
                u32 soft_id;
                u32 rigid_id;
            };            

            BodyIdData body_id_data;

            void init()
            {
                body_id_data = {0, 0};
            }

            PhysBodyId genRigidBodyId()
            {
                u32 id = ++body_id_data.rigid_id;
                id |= 0x80000000;
                return id;
            }

            PhysBodyId genSoftbodyId()
            {
                u32 id = ++body_id_data.soft_id;
                return id;
            }         

            b32 validId(PhysBodyId id) 
            {
                return id & 0x7FFFFFFF;
            }

            b32 isRigidBody(PhysBodyId id)
            {
                return id & 0x80000000;
            }
            
            b32 isSoftBody(PhysBodyId id)
            {
                return !(id & 0x80000000);
            }
        }
    }
}