#pragma once

#include <typedefs.h>

namespace RED
{
    namespace sim
    {
        namespace grab
        {
            struct Grab
            {
                b32 is_active = false;

                math::uv3 grabbed_vert_indices;
                math::v3 grabbed_pos_bary_coords;

                u32 grabbed_body_id;
                f32 dist_along_ray;
                RED::math::v3 current_hand_pos;
                f32 hand_inv_mass;
            };                    

            math::v3 getGrabPos(Grab *grab, math::v3* pos)
            {
                math::v3 p1 = pos[grab->grabbed_vert_indices.x];
                math::v3 p2 = pos[grab->grabbed_vert_indices.y];
                math::v3 p3 = pos[grab->grabbed_vert_indices.z];

                math::v3 res = baryCoordsToPos(grab->grabbed_pos_bary_coords, p1, p2, p3);
            
                return res;
            }     
        }
    }
}