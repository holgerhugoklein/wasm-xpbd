#pragma once

#include <unordered_set>

#include <typedefs.h>

#include "./intersection/aabb.h"

#include "./phyiscs_bodies/rigid_body.h"

//#include "./sim_debug.h"

#include "../maths/maths.h"

namespace RED
{
    namespace sim
    {
        namespace spat_grid
        {
            struct AABBSpatialHashGrid
            {
                const f32 _spacing = 2.;
                static const u32 _max_num_entries = 1024 * 8 * 8;
                static const u32 _table_size = 2 * _max_num_entries;

                RigidBody **_rb_ptr_in_cell = (RigidBody **) malloc(_max_num_entries * sizeof(RigidBody *));
                u32 *_cell_entries_start = (u32 *) malloc((_table_size + 1) * sizeof(u32));
                

                math::uv3 cell(math::v3 p)
                {
                    return glm::floor(p / _spacing);
                }   

                u32 hash(math::uv3 c)
                {
                    return ((c.x * 92837111) ^ (c.y * 689287499) ^ (c.z * 283923481)) % _table_size;	
                }

                void clear()
                {
                    memset(_cell_entries_start, 0, _table_size * sizeof(u32));
                    memset(_rb_ptr_in_cell, 0, _max_num_entries * sizeof(RigidBody *));
                }

                void calc_presum(aabb::AABB *aabbs, u32 amt_rbs)
                {
                    for(u32 i = 0; i < amt_rbs; ++i)
                    {
                        aabb::AABB aabb = aabbs[i];
                        math::uv3 steps = glm::ceil((aabb.max - aabb.min) / _spacing);
                        for(u32 x = 0; x < steps.x; ++x)
                        {
                            for(u32 y = 0; y < steps.y; ++y)
                            {
                                for(u32 z = 0; z < steps.z; ++z)
                                {
                                    math::v3 pos = aabb.min + math::v3(x, y, z) * _spacing;
                                    u32 h = hash(pos);
                                    _cell_entries_start[h]++;
                                }
                            }
                        }
                    }

                    u32 sum = 0;
                    for(u32 i = 0; i < _table_size; ++i)
                    {
                        sum += _cell_entries_start[i];
                        _cell_entries_start[i] = sum;   
                        assert(sum < _max_num_entries);
                    }
                    _cell_entries_start[_table_size] = sum;
                }

                void insert(RigidBody *rb, aabb::AABB aabb)
                {
                    math::uv3 steps = glm::ceil((aabb.max - aabb.min) / _spacing);
                    for(u32 x = 0; x < steps.x; ++x)
                    {
                        for(u32 y = 0; y < steps.y; ++y)
                        {
                            for(u32 z = 0; z < steps.z; ++z)
                            {
                                math::v3 pos = aabb.min + math::v3(x, y, z) * _spacing;
                                u32 h = hash(pos);
                                _cell_entries_start[h]--;
                                _rb_ptr_in_cell[_cell_entries_start[h]] = rb;
                            }
                        }
                    }
                }

                std::unordered_set<RigidBody *> query(aabb::AABB query)
                {
                    std::unordered_set<RigidBody *> res{};
                    res.reserve(32);
                    math::uv3 steps = glm::ceil((query.max - query.min) / _spacing);

                    for(u32 x = 0; x < steps.x; ++x)
                    {
                        for(u32 y = 0; y < steps.y; ++y)
                        {
                            for(u32 z = 0; z < steps.z; ++z)
                            {
                                math::v3 pos = query.min + math::v3(x, y, z) * _spacing;
                                u32 h = hash(pos);
                                u32 begin = _cell_entries_start[h];
                                u32 amt = _cell_entries_start[h + 1] - begin;
                                for(u32 i = 0; i < amt; ++i)
                                {
                                    res.insert(_rb_ptr_in_cell[begin + i]);
                                }
                            }
                        }
                    }

                    return res;
                }
            };
        }
    }    
}
