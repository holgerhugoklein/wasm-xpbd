#pragma once

#include <typedefs.h>

#include "./intersection/collisions.h"

#include "./sim.h"
//#include "./sim_debug.h"



namespace RED
{
    namespace sim
    {
        void storePrevStateAndApplyForces(Sim *sim, f32 dtS)
        {
            for(u32 i = 0; i < sim->amt_rigid_bodies; ++i)
            {
                RigidBody *rb = sim->rigid_bodies + i;
                
                rb->prev_pos = rb->pos;
                rb->prev_quat = rb->quat;

                if(rb->inv_mass == 0.) continue;

                if(sim->grav) rb->velocity += dtS * G;
                
                rb->pos += dtS * rb->velocity;
                math::quat d_quat = dtS * .5f * math::quatFromAngularVel(rb->angular_vel)*rb->quat;
                updateQuat(rb, rb->quat + d_quat);
            }
        }

        void applyRigidBodyParticlePositionalCorrection(
            RigidBody *rb, ParticleRef p, math::v3 r_rotated, 
            f32 rest_dist, f32 alpha, math::v3 n, f32 c)
        {

            f32 w = calcGenInverseMassInDir(rb, r_rotated, n);
            //TODO figure out what the deltate lagrange stuff is all about

            f32 lambda = -c / (w + p.w + alpha);
            math::v3 P = lambda * n;

            //debug::debugPushLineInDir({.5, .5, 0.}, pos_in_world, P);       
            
            rb->pos += P * rb->inv_mass;
            //*p.pos -= P * p.w;

            math::quat d_quat = .5f * math::quatFromAngularVel(rb->current_rot_data.current_I_inv * (glm::cross(r_rotated, P))) * rb->quat;
            updateQuat(rb, rb->quat + d_quat);
        }

        void solveRigidBodyParticleDistanceConstraint(
            RigidBody *rb, ParticleRef p, math::v3 r_rotated, 
            f32 rest_dist, f32 alpha)
        {
            //move pos_on_body into world coordinates
            math::m4 body_m4 = toM4(rb);
            math::v3 pos_in_world = r_rotated + rb->pos;
            math::v3 n = *p.pos - pos_in_world;

            f32 len = glm::length(n);
            f32 err = rest_dist - len;

            if(len == 0.)
            {
                if(err == 0.)
                {
                    return;
                }
                else
                {
                    assert(false);
                }
            }
            else
            {
                n /= len;
            }

            applyRigidBodyParticlePositionalCorrection(rb, p, r_rotated, rest_dist, alpha, n, err);
        }

        void applyTwoRigidBodiesPositionalCorrection(
            RigidBody *lhs, RigidBody *rhs, 
            math::v3 r1_rot, math::v3 r2_rot, 
            math::v3 n, f32 c, f32 alpha)
        {
           
            f32 w1 = calcGenInverseMassInDir(lhs, r1_rot, n);
            f32 w2 = calcGenInverseMassInDir(rhs, r2_rot, n);
       
            f32 lambda = (-c - alpha) / (w1 + w2 + alpha);

            math::v3 P = lambda * n;

            lhs->pos += P * lhs->inv_mass;
            rhs->pos -= P * rhs->inv_mass;

            math::quat dq1 = .5f * math::quatFromAngularVel(lhs->current_rot_data.current_I_inv * (glm::cross(r1_rot, P))) * lhs->quat;
            math::quat dq2 = .5f * math::quatFromAngularVel(rhs->current_rot_data.current_I_inv * (glm::cross(r2_rot, P))) * rhs->quat;

            updateQuat(lhs, lhs->quat + dq1);
            updateQuat(rhs, rhs->quat - dq2);
        }

        f32 calcTotalLinVelInNormalDir(RigidBody *lhs, RigidBody *rhs, math::v3 n)
        {  
            return glm::dot(lhs->velocity - rhs->velocity, n);
        }

        f32 calcTotalRotVelInNormalDir(RigidBody *lhs, RigidBody *rhs, math::v3 r1_rot, math::v3 r2_rot, math::v3 n)
        {
            math::v3 v_tot = glm::cross(lhs->angular_vel, r1_rot) - glm::cross(rhs->angular_vel, r2_rot);
            return glm::dot(v_tot, n);
        }    


        void solvePositions(Sim *sim, coll::CollisionArr *out_colls, f32 dtS)
        {
            grab::Grab *grab = sim->grab_ptr;
            if(grab->is_active)
            {
                RigidBody *rb = getRigidBodyFromId(sim, grab->grabbed_body_id);
                math::v3 r_rotated = rb->current_rot_data.current_rot_mat * (rb->scaling * grab::getGrabPos(grab, rb->ref->vert_positions));

                ParticleRef ref{};
                ref.pos = &grab->current_hand_pos;
                ref.w = grab->hand_inv_mass;

                solveRigidBodyParticleDistanceConstraint(rb, ref, r_rotated, 0., 0.);
            }
#if 0
            coll::findAllCurrentCollisions(sim, out_colls);

            for(u32 i = 0; i < out_colls->amt; )
            {
                coll::Collision c = out_colls->colls[i];
                math::v3 p1 = c.lhs->current_rot_data.current_rot_mat * c.epa_res.r1 + c.lhs->pos;
                math::v3 p2 = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2 + c.rhs->pos;
                f32 d = glm::dot(p1 - p2, c.epa_res.n);
                if(d > 0.)
                {
                    math::v3 n = (p1 - p2) / d;
                    c.used_n = n;
                    c.vn_prev = calcTotalContactVelInNormalDir(c.lhs, c.rhs, p1 - c.lhs->pos, p2 - c.rhs->pos, n);
                    applyTwoRigidBodiesPositionalCorrection(c.lhs, c.rhs, p1, p2, n, d, 0.);        
                    p1 = c.lhs->current_rot_data.current_rot_mat * c.epa_res.r1;
                    p2 = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2;
                    c.vn_post = calcTotalContactVelInNormalDir(c.lhs, c.rhs, p1, p2, n);
                    ++i;
                }                 
                else
                {
                    out_colls->remove_at(i);
                }
                //debug::inputAndRender();
            }
#endif
            

            for(u32 i = 0; i < sim->amt_rigid_bodies; ++i)
            {
                RigidBody *rb = sim->rigid_bodies + i;

                if(rb->inv_mass == 0.) continue;

                coll::Collision with_walls[6];

                u32 amt = coll::findPossibleCollsWithWalls(rb, sim, with_walls);
                for(u32 j = 0; j < amt; ++j)
                {
                    coll::Collision c = with_walls[j];
                    math::v3 rb_prev_pos = rb->pos;

                    if(c.epa_res.c > 1e-5)
                    {
                        math::v3 r1_rot = rb->current_rot_data.current_rot_mat * c.epa_res.r1;
                        math::v3 r2_rot = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2;
                        c.vn_lin_prev = calcTotalLinVelInNormalDir(rb, c.rhs, c.epa_res.n);
                        c.vn_rot_prev = calcTotalRotVelInNormalDir(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n);
                        applyTwoRigidBodiesPositionalCorrection(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n, c.epa_res.c, 0.);   
                    }
                    c.lhs_gained_vel = (rb->pos - rb_prev_pos) / dtS;
                    out_colls->push_back(c);
                }
            }
                
            sim->aabb_hash_grid.clear();
            sim->aabb_hash_grid.insert(sim->cube_colliders.size(), sim->rigid_bodies + sim->wall_colliders.size());

          
            for(RigidBody *rb = sim->rigid_bodies + sim->wall_colliders.size(); rb != sim->rigid_bodies + sim->amt_rigid_bodies; ++rb)
            {
                aabb::AABB rb_aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);
                const auto poss_colls = sim->aabb_hash_grid.query(rb_aabb);
                
                math::v3 avg_n(0.);

                for(RigidBody *rhs : poss_colls)
                {
                    aabb::AABB rhs_aabb = aabb::calcAABB(toM4(rhs), rhs->ref->vert_positions, rhs->ref->amt_verts);
                    if(!aabb::overlapp(rb_aabb, rhs_aabb)) continue;                    
                    gjk::EPARes res;
                    if(gjk::performRigidBodyGJK(rb, rhs, &res))
                    {
                        coll::Collision c = {res, rb, rhs};

                        math::v3 rb_prev_pos = rb->pos;
                        math::v3 rhs_prev_pos = rhs->pos;

                        if(c.epa_res.c > 1e-5)
                        {
                            math::v3 r1_rot = rb->current_rot_data.current_rot_mat * c.epa_res.r1;
                            math::v3 r2_rot = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2;
                            c.vn_lin_prev = calcTotalLinVelInNormalDir(rb, c.rhs, c.epa_res.n);
                            c.vn_rot_prev = calcTotalRotVelInNormalDir(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n);
                            applyTwoRigidBodiesPositionalCorrection(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n, c.epa_res.c, 0.);
                        }

                        c.lhs_gained_vel = (rb->pos - rb_prev_pos) / dtS;
                        c.rhs_gained_vel = (rhs->pos - rhs_prev_pos) / dtS;
                        
                        out_colls->push_back(c);
                    }
                }
            }            
#if 0
            for(u32 i = 0; i < sim->amt_rigid_bodies; ++i)
            {
                RigidBody *rb = sim->rigid_bodies + i;

                if(rb->inv_mass == 0.) continue;

                aabb::AABB rb_prev_aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);
                aabb::AABB rb_aabb;
                const auto poss_coll_ids = sim->aabb_bucket_hash_grid.query(rb->id, rb_prev_aabb);

                u32 iter = 0;
                for(const u32 rhs_id : poss_coll_ids)
                {                    
                    rb_aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);

                    RigidBody *rhs = getRigidBodyFromId(sim, rhs_id);

                    //debug::pushRigidBodyAABB({1, 0, 0}, rhs);
                    
                    aabb::AABB rhs_aabb_prev = aabb::calcAABB(toM4(rhs), rhs->ref->vert_positions, rhs->ref->amt_verts);
                    if(!aabb::overlapp(rb_aabb, rhs_aabb_prev)) continue;                    

                    gjk::EPARes res;
                    if(gjk::performRigidBodyGJK(rb, rhs, &res))
                    {
                        coll::Collision c = {res, rb, rhs};
                        if(c.epa_res.c > 1e-5)
                        {
                            math::v3 r1_rot = rb->current_rot_data.current_rot_mat * c.epa_res.r1;
                            math::v3 r2_rot = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2;
                            c.vn_lin_prev = calcTotalLinVelInNormalDir(rb, c.rhs, c.epa_res.n);
                            c.vn_rot_prev = calcTotalRotVelInNormalDir(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n);
                            applyTwoRigidBodiesPositionalCorrection(rb, c.rhs, r1_rot, r2_rot, c.epa_res.n, c.epa_res.c, 0.);
                        }

                        aabb::AABB rhs_aabb = aabb::calcAABB(toM4(rhs), rhs->ref->vert_positions, rhs->ref->amt_verts);
                        sim->aabb_bucket_hash_grid.update(rhs_id, rhs_aabb_prev, rhs_aabb);
                        
                        out_colls->push_back(c);
                    }
                }

                sim->aabb_bucket_hash_grid.update(rb->id, rb_prev_aabb, rb_aabb);
            }
#endif
        }   

        void updateVelocities(Sim *sim, f32 dtS)
        {
            for(RigidBody *rb = sim->rigid_bodies; rb != sim->rigid_bodies + sim->amt_rigid_bodies; ++rb)
            {
                rb->velocity = (rb->pos - rb->prev_pos) / dtS;

                math::quat dq = rb->quat * glm::inverse(rb->prev_quat);
                rb->angular_vel = 2.f * (math::v3(dq.x, dq.y, dq.z)) / dtS;
                
                if(dq.w < 0.) rb->angular_vel = -rb->angular_vel;
            }
        }
        
        void applySeparateVelocityCorrection(RigidBody *lhs, RigidBody *rhs, math::v3 r1_rot, math::v3 r2_rot, math::v3 n, f32 dv_lin, f32 dv_rot)
        {        
            f32 rot_mass_1 = calcRotationalMassInDir(lhs, r1_rot, n);
            f32 rot_mass_2 = calcRotationalMassInDir(rhs, r2_rot, n);

            math::v3 P_lin = (dv_lin * n) / (lhs->inv_mass + rhs->inv_mass);
            math::v3 P_rot = (dv_rot * n) / (rot_mass_1 + rot_mass_2);

            lhs->velocity += P_lin * lhs->inv_mass;
            rhs->velocity -= P_lin * rhs->inv_mass;           

            lhs->angular_vel += lhs->current_rot_data.current_I_inv * glm::cross(r1_rot, P_rot);
            rhs->angular_vel -= rhs->current_rot_data.current_I_inv * glm::cross(r2_rot, P_rot);
        }

        void applyVelocityCorrection(RigidBody *lhs, RigidBody *rhs, math::v3 r1_rot, math::v3 r2_rot, math::v3 n, f32 dv)
        {        
            f32 rot_mass_1 = calcRotationalMassInDir(lhs, r1_rot, n);
            f32 rot_mass_2 = calcRotationalMassInDir(rhs, r2_rot, n);

            math::v3 P = (dv * n) / (lhs->inv_mass + rhs->inv_mass + rot_mass_1 + rot_mass_2);

            lhs->velocity += P * lhs->inv_mass;
            rhs->velocity -= P * rhs->inv_mass;           

            lhs->angular_vel += lhs->current_rot_data.current_I_inv * glm::cross(r1_rot, P);
            rhs->angular_vel -= rhs->current_rot_data.current_I_inv * glm::cross(r2_rot, P);
        }

        void solveVelocities(Sim *sim, coll::CollisionArr colls, f32 dtS)
        {           
            for(u32 i = 0; i < colls.colls.size(); ++i)
            {
                coll::Collision c = colls.colls[i];

                math::v3 r1_rot = c.lhs->current_rot_data.current_rot_mat * c.epa_res.r1;
                math::v3 r2_rot = c.rhs->current_rot_data.current_rot_mat * c.epa_res.r2;

                f32 vn_lin_post = calcTotalLinVelInNormalDir(c.lhs, c.rhs, c.epa_res.n);
                f32 vn_rot_post = calcTotalRotVelInNormalDir(c.lhs, c.rhs, r1_rot, r2_rot, c.epa_res.n);

                f32 dv_lin = -vn_lin_post;
                f32 dv_rot = -vn_rot_post;                

                c.lhs->velocity -= c.lhs_gained_vel;
                c.rhs->velocity -= c.rhs_gained_vel;


                applySeparateVelocityCorrection(
                    c.lhs, c.rhs, 
                    r1_rot, 
                    r2_rot,
                    c.epa_res.n, 
                    0.f, dv_rot
                );
           
                f32 e = .3;
                applyVelocityCorrection(
                    c.lhs, c.rhs, 
                    r1_rot, 
                    r2_rot,
                    c.epa_res.n, 
                    std::min(e * -(c.vn_lin_prev + c.vn_rot_prev), 0.f)
                );               
            }
        }

        void stepSimulation(Sim *sim, f32 dtS)
        {
            storePrevStateAndApplyForces(sim, dtS);
            coll::CollisionArr colls{};
            solvePositions(sim, &colls, dtS);
            updateVelocities(sim, dtS);
            solveVelocities(sim, colls, dtS);
        }
        
        void simOneFrame(Sim *sim, f32 dtS, render::scene::Scene *scene)
        {
            scene->debug_rects.clear();
            scene->debug_lines.clear();
            scene->debug_points.clear();
            scene->debug_triangles.clear();
#if 0
            sim->aabb_bucket_hash_grid.clear();
            for(u32 i = 0; i < sim->cube_colliders.size(); ++i)
            {
                RigidBody *rb = sim->cube_colliders[i].rb;
                aabb::AABB aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);
                sim->aabb_bucket_hash_grid.insert(rb->id, aabb);
            }
#endif
           
            u32 n = 5;
            f32 dtS_n = (dtS) / (f32) n;

            for(u32 i = 0; i < n; ++i)
            {
                stepSimulation(sim, dtS_n);
            }            
        }          
    }
}

