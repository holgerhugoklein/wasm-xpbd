#pragma once

#include <unordered_set>
#include <vector>
#include <algorithm>
#include <iostream>

#include <typedefs.h>

#include "./collider.h"

//#include "../sim_debug.h"

#include "../phyiscs_bodies/rigid_body.h"

#include "../../maths/maths.h"

#include "../ray_triangle.h"



namespace RED
{
    namespace sim
    {
        namespace gjk
        { 

            struct PointOnMinkowskiHull
            {
                math::v3 p;
                math::v3 p_lhs;
                math::v3 p_rhs;
            };

            struct Simplex
            {                
                u32 dim;
                union 
                {
                    struct {
                        PointOnMinkowskiHull p1;
                        PointOnMinkowskiHull p2;
                        PointOnMinkowskiHull p3;
                        PointOnMinkowskiHull p4;
                    };
                    PointOnMinkowskiHull points[4];
                };                
            };            

            math::v3 findFurthestPoint(RigidBody *rb, math::m4 transform, math::v3 dir)
            {
                f32 max = -std::numeric_limits<f32>::max();
                math::v3 res;
                for(u32 i = 0; i < rb->ref->amt_verts; ++i)
                {
                    math::v3 p = math::applyTransform(transform, rb->ref->vert_positions[i]);
                    f32 d = glm::dot(p, dir);   
                    if(d > max)
                    {
                        max = d;
                        res = p;
                    }
                }
                return res;
            }


            PointOnMinkowskiHull findSupportPoint(
                    RigidBody *lhs,
                    RigidBody *rhs,
                    math::m4 lhs_transform,
                    math::m4 rhs_transform,
                    math::v3 dir
                    )
            {
                math::v3 lhs_p = findFurthestPoint(lhs, lhs_transform, dir);
                math::v3 rhs_p = findFurthestPoint(rhs, rhs_transform, -dir);
                 
                return {lhs_p - rhs_p, lhs_p, rhs_p};
            }

            b32 sameDir(math::v3 d1, math::v3 d2)
            {
                return glm::dot(d1, d2) > 0.;
            }

            struct U32F
            {
                u32 u;
                f32 f;
            };

            struct Edges
            {
                u32 amt_edges = 0;
                math::uv2 edges[128];
            
                void add(math::uv2 edge)
                {
                    edges[amt_edges++] = edge;
                }

                b32 contains_reverse_remove(math::uv2 e)
                {
                    for(u32 i = 0; i < amt_edges; ++i)
                    {
                        if(edges[i].x == e.y && edges[i].y == e.x)
                        {
                            edges[i] = edges[amt_edges - 1];
                            amt_edges--;
                            return true;
                        }
                    }
                    return false;
                }
            };
       
            struct Polytope
            {
                u32 amt_pos;
                u32 amt_faces;
                PointOnMinkowskiHull pos_on_hull[512];
                math::v3 face_normals[512];
                math::uv3 indices[512];

                void addVertex(PointOnMinkowskiHull p)
                {
                    pos_on_hull[amt_pos++] = p;
                }

                void addFace(math::uv3 ind)
                {
                    indices[amt_faces] = ind;
                    math::v3 normal = glm::normalize(glm::cross(
                        pos_on_hull[ind.y].p - pos_on_hull[ind.x].p, 
                        pos_on_hull[ind.z].p - pos_on_hull[ind.x].p));

                    face_normals[amt_faces] = normal;
                    amt_faces++;
                }

                void drawdebug()
                {
                    for(const auto ind : indices)
                    {
                        //debug::pushTriangle(math::v3(0, 1, 0), pos_on_hull[ind.x].p, pos_on_hull[ind.y].p, pos_on_hull[ind.z].p);
                    }
                }

                U32F closestFaceToOrigin()
                {
                    f32 min = std::numeric_limits<f32>::max();
                    u32 closest_idx = 0;                    
                    for(u32 i = 0; i < amt_faces; ++i)
                    {
                        math::v3 n = face_normals[i];
                        f32 dist = glm::dot(n, pos_on_hull[indices[i].x].p);
                        if(dist < min)
                        {
                            min = dist;
                            closest_idx = i;
                        }
                    }
                    return {closest_idx, min};
                }

                void insertVertex(PointOnMinkowskiHull p)
                {
                    Edges edges{};         

                    for(u32 i = 0; i < amt_pos; ++i)
                    {
                        if(glm::length(pos_on_hull[i].p - p.p) < 1e-5) return;
                    }         
                    
                    for(u32 i = 0; i < amt_faces;)
                    {
                        math::v3 n = face_normals[i];
                        math::uv3 ind = indices[i];

                        if(sameDir(pos_on_hull[ind.x].p - p.p, -n))
                        {   
                            face_normals[i] = face_normals[amt_faces - 1];
                            indices[i] = indices[amt_faces - 1];
                            amt_faces--;

                            u32 amt_new_edges = 3;
                            math::uv2 new_edges[3] = 
                            {
                                {ind.x, ind.y},
                                {ind.y, ind.z},
                                {ind.z, ind.x},
                            };
                            for(u32 k = 0; k < 3; ++k)
                            {
                                if(edges.contains_reverse_remove(new_edges[k]))
                                {
                                }
                                else
                                {
                                    edges.add(new_edges[k]);
                                }
                            }                            
                        }
                        else
                        {
                            ++i;
                        }                        
                    }
                    
                    //drawdebug();
                    //debug::inputAndRender(true);

                    addVertex(p);

                    for(u32 i = 0; i < edges.amt_edges; ++i)
                    {
                        addFace({edges.edges[i].x, edges.edges[i].y, amt_pos - 1});
                    }
                    
                    //drawdebug();
                    //debug::inputAndRender(true);
                }
            };               

            struct EPARes
            {
                math::v3 n;
                f32 c;
                math::v3 r1;
                math::v3 r2;
            };

            b32 runEPA(Simplex simplex, RigidBody *lhs, RigidBody *rhs, math::m4 lhs_transform, math::m4 rhs_transform, EPARes *res)
            {
                Polytope poly{};

                poly.addVertex(simplex.p1);
                poly.addVertex(simplex.p2);
                poly.addVertex(simplex.p3);
                poly.addVertex(simplex.p4);

                poly.addFace({3, 0, 1});
                poly.addFace({3, 1, 2});
                poly.addFace({3, 2, 0});
                poly.addFace({2, 1, 0});

                //poly.drawdebug();

                //debug::inputAndRender(true);
                //debug::inputAndRender(true);

                for(u32 iter = 0;; iter++)
                {
                    if(iter == 10)
                    {
                        return false;
                    }
                    U32F closest_face_data = poly.closestFaceToOrigin();
                    u32 closest_face_idx = closest_face_data.u;
                    f32 closest_face_dist = closest_face_data.f;

                    math::v3 closest_face_normal = poly.face_normals[closest_face_idx];

                    //debug::debugPushRectAroundPoint(math::v3(1, 0, 0), closest_face_dist * closest_face_normal);
                    //debug::inputAndRender(true);
                   
                    PointOnMinkowskiHull support_in_face_dir = findSupportPoint(lhs, rhs, lhs_transform, rhs_transform, closest_face_normal);
                    //debug::debugPushPoint(math::v3(1, 0, 0), support_in_face_dir.p);

                    f32 dist_support_point = glm::dot(closest_face_normal, support_in_face_dir.p);

                    if(dist_support_point - closest_face_dist < 1e-6)
                    {
                        //TODO move into body frame
                        math::v3 closest_pos_on_hull = dist_support_point * closest_face_normal;
                        
                        math::uv3 face_indices = poly.indices[closest_face_idx];
                        PointOnMinkowskiHull face_p1 = poly.pos_on_hull[face_indices.x];
                        PointOnMinkowskiHull face_p2 = poly.pos_on_hull[face_indices.y];
                        PointOnMinkowskiHull face_p3 = poly.pos_on_hull[face_indices.z];                        

                        math::v3 bary = findBaryCoords(closest_pos_on_hull, face_p1.p, face_p2.p, face_p3.p);

                        math::v3 r1 = bary.x * face_p1.p_lhs + bary.y * face_p2.p_lhs + bary.z * face_p3.p_lhs;
                        math::v3 r2 = bary.x * face_p1.p_rhs + bary.y * face_p2.p_rhs + bary.z * face_p3.p_rhs;

                        res->n = closest_face_normal;
                        res->c = dist_support_point;
                        res->r1 = pointToBodyFrame(lhs, r1);
                        res->r2 = pointToBodyFrame(rhs, r2);

                        //assert(abs(glm::length(r1 - r2) - dist_sup    port_point) < 1e-6);
                        
                        //debug::debugPushPoint({1, 0, 0}, r1);
                        //debug::debugPushPoint({1, 0, 0}, r2);
                        //lhs->color.a = .1;
                        //rhs->color.a = .1;
                        //debug::inputAndRender(true);
                        return true;
                    }
                    else
                    {
                        poly.insertVertex(support_in_face_dir);
                        //poly.drawdebug();
                        //debug::inputAndRender(true);
                    }
                }
            }

            
                     
            b32 performRigidBodyGJK(
                    RigidBody *lhs,
                    RigidBody *rhs,
                    EPARes *res
                )
            {
                math::m4 lhs_transform = toM4(lhs);
                math::m4 rhs_transform = toM4(rhs);
                math::v3 search_dir = {-1, 0, 0};

                math::v3 lhs_p;
                math::v3 rhs_p;
                PointOnMinkowskiHull A = findSupportPoint(lhs, rhs, lhs_transform, rhs_transform, search_dir);

                Simplex simplex{};
                simplex = {1, A};

                search_dir = -A.p;

                math::v3 origin_projected;
            
                
                for(u32 iter = 0;;++iter)
                {                
                    //debug::debugPushPoint(math::v3(0.), math::v3(0.));
                    if(iter == 6) return false;
                    math::v3 lhs_p;
                    math::v3 rhs_p;
                    A = findSupportPoint(lhs, rhs, lhs_transform, rhs_transform, search_dir);
                    math::v3 AO = -A.p;
                    PointOnMinkowskiHull B = simplex.p1;
                    math::v3 AB = B.p - A.p;

                    if(glm::dot(A.p, search_dir) < 0.)
                    {
                        return false;
                    }

                    if(simplex.dim == 1)
                    {   
                        search_dir = glm::cross(AB, glm::cross(AO, AB));                        
                        simplex = {2, B, A};                     
                        //debug::debugPushLineInDir(math::v3(1, 0, 0), A, AB);
                        //debug::debugPushLineInDir(math::v3(1, 0, 0), A + .5f * AB, search_dir);
                        //debug::inputAndRender(true);
                    }                    
                    else if(simplex.dim == 2)
                    {                            
                        PointOnMinkowskiHull C = simplex.p2;
                        math::v3 AC = C.p - A.p;
                        math::v3 n = glm::cross(AB, AC);
                        if(!sameDir(AO, n))
                        {
                            simplex = {3, C, B, A};
                            search_dir = -n;
                        }
                        else
                        {
                            simplex = {3, B, C, A};
                            search_dir = n;
                        }                        
                        //debug::pushTriangle(math::v3(1,0,0), simplex.p1, simplex.p2, simplex.p3);
                        //debug::debugPushLineInDir(math::v3(1, 0, 0), A + .3f * AB + .3f * AC, search_dir);
                        //debug::inputAndRender();
                    }
                    else if(simplex.dim == 3)
                    {          
                        PointOnMinkowskiHull C = simplex.p2;
                        PointOnMinkowskiHull D = simplex.p3;
                        math::v3 AC = C.p - A.p;    
                        math::v3 AD = D.p - A.p;            

                        math::v3 n_ABC = glm::cross(AB, AC);
                        math::v3 n_ACD = glm::cross(AC, AD);
                        math::v3 n_ADB = glm::cross(AD, AB);

                        //debug::pushTriangle(math::v3(1, 0, 0), A, B, C);
                        //debug::pushTriangle(math::v3(1, 0, 0), A, C, D);
                        //debug::pushTriangle(math::v3(1, 0, 0), A, D, B);
                        //debug::pushTriangle(math::v3(1, 0, 0), B, C, D);

                        if(sameDir(AO, n_ABC))
                        {
                            simplex = {3, A, B, C};
                            search_dir = n_ABC;
                        }
                        else if(sameDir(AO, n_ACD))
                        {
                            simplex = {3, A, C, D};
                            search_dir = n_ACD;
                        }
                        else if(sameDir(AO, n_ADB))
                        {
                            simplex = {3, A, D, B};
                            search_dir = n_ADB;
                        }
                        else
                        {
                            simplex = {4, B, C, D, A};
                            break;
                        }

                        //debug::debugPushLineInDir(math::v3(1, 0, 0), A, search_dir);
                        //debug::inputAndRender();
                    }
                }                
                //debug::inputAndRender();
                //debug::pushRigidBodyAABB(math::v3(1, 0, 0), lhs);

                return runEPA(simplex, lhs, rhs, lhs_transform, rhs_transform, res);            
            }           
        }
    }
}