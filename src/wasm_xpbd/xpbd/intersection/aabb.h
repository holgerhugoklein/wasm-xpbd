#pragma once

#include <typedefs.h>

#include "../../maths/maths.h"

namespace RED
{
    namespace sim
    {
        namespace aabb
        {
            struct AABB
            {
                math::v3 min;
                math::v3 max;
            };

            b32 overlapp(AABB lhs, AABB rhs)
            {
                if(lhs.max.x < rhs.min.x) return false;
                if(lhs.max.y < rhs.min.y) return false;
                if(lhs.max.z < rhs.min.z) return false;

                
                if(rhs.max.x < lhs.min.x) return false;
                if(rhs.max.y < lhs.min.y) return false;
                if(rhs.max.z < lhs.min.z) return false;
            
                return true;
            }

            AABB cut(AABB lhs, AABB rhs)
            {
                math::v3 min = glm::max(lhs.min, rhs.min);
                math::v3 max = glm::min(lhs.max, rhs.max);
                return {min, max};
            }

            AABB calcAABB(math::v3 *pos, u32 amt_pos)
            {
                math::v3 min;
                min.x = std::numeric_limits<f32>::max();
                min.y = std::numeric_limits<f32>::max();
                min.z = std::numeric_limits<f32>::max();

                math::v3 max = -min;

                for(u32 i = 0; i < amt_pos; ++i)
                {
                    math::v3 p = pos[i];
                    min = glm::min(min, p);
                    max = glm::max(max, p);
                }

                AABB aabb{};
                aabb.min = min;
                aabb.max = max;

                return aabb;
            }

             AABB calcAABB(math::m4 transform, math::v3 *pos, u32 amt_pos)
            {
                math::v3 min;
                min.x = std::numeric_limits<f32>::max();
                min.y = std::numeric_limits<f32>::max();
                min.z = std::numeric_limits<f32>::max();

                math::v3 max = -min;

                for(u32 i = 0; i < amt_pos; ++i)
                {
                    math::v3 p = math::applyTransform(transform, pos[i]);
                    min = glm::min(min, p);
                    max = glm::max(max, p);
                }

                AABB aabb{};
                aabb.min = min;
                aabb.max = max;

                return aabb;
            }
        }        
    }
}
  
