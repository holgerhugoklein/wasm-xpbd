#pragma once

#include <typedefs.h>

#include "./aabb.h"

#include "../maths/maths.h"


namespace RED
{
    namespace sim
    {
        namespace aabb
        {
            namespace grid
            {
                struct AABBSpatialGridASDASDASD
                {
                    const f32 _spacing = .5;
                    const math::v3 _zero = math::v3(-16);
                    const math::v3 _dims = math::v3(32);
                    const math::uv3 _amt_cells_in_dim = glm::ceil(_dims / _spacing);
                    const u32 _total_amt_cells = _amt_cells_in_dim.x * _amt_cells_in_dim.y * _amt_cells_in_dim.z;
                    const u32 _MAX_ENTRIES_CELLS = 512;
                    u32 *_cell_entries;
                    u32 *_amt_entries_per_cell;         
                    
                    math::uv3 cell(math::v3 p)
                    {
                        return glm::floor((p - _zero) / _spacing);
                    }

                    u32 cell_to_index(math::uv3 cell)
                    {
                        return cell.z * (_amt_cells_in_dim.y * _amt_cells_in_dim.x) + cell.y * _amt_cells_in_dim.x + cell.x;
                    }   

                    void insert(u32 id, AABB aabb)
                    {
                        math::uv3 steps = glm::ceil((aabb.max - aabb.min) / _spacing);
                        for(u32 z = 0; z < steps.z; ++z)
                        {
                            f32 z_off = z * _spacing;
                            for(u32 y = 0; y < steps.y; ++y)
                            {
                                f32 y_off = y * _spacing;
                                for(u32 x = 0; x < steps.x; ++x)
                                {
                                    f32 x_off = x * _spacing;
                                    math::v3 p = aabb.min + math::v3(x_off, y_off, z_off);
                                    math::uv3 c = cell(p);
                                    u32 idx = cell_to_index(c);
                                    u32 amt = _amt_entries_per_cell[idx];
                                    assert(amt < _MAX_ENTRIES_CELLS);
                                    _cell_entries[idx * _MAX_ENTRIES_CELLS + amt] = id;
                                    _amt_entries_per_cell[idx]++;
                                }
                            }
                        }
                    }
                };  

                AABBSpatialGridASDASDASD init()
                {
                    AABBSpatialGridASDASDASD res{};
                    res._cell_entries = 
                        (u32 *) malloc(
                            res._total_amt_cells * 
                            res._MAX_ENTRIES_CELLS * sizeof(u32));
                    res._amt_entries_per_cell = (u32 *) malloc(
                            res._total_amt_cells * sizeof(u32));
                    memset(res._amt_entries_per_cell, 0, res._total_amt_cells * sizeof(u32));
                    return res;
                }          
            }            
        }        
    }
}
  
