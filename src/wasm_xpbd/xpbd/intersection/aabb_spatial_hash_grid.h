#pragma once

#include <unordered_set>

#include <typedefs.h>

#include "./aabb.h"

//#include "../sim_debug.h"

#include "../../maths/maths.h"

#include "../../memory/memory.h"

namespace RED
{
   
    namespace sim
    {
        namespace aabb
        {
            const u32 MAX_NUM_ENTRIES = 2048 * 8;
            const u32 TABLE_SIZE = MAX_NUM_ENTRIES * 2;
            const f32 SPACING = .5;

            struct AABBHashGrid
            {
                
                u32 *cell_start_indices;
                RigidBody **cell_entries;

                math::iv3 cell(math::v3 p)
                {
                    return glm::floor(p / SPACING);
                }   

                u32 hash(math::uv3 c)
                {
                    return ((c.x * 92837111) ^ (c.y * 689287499) ^ (c.z * 283923481)) % TABLE_SIZE;	
                }

                std::vector<u32> enumerateCells(aabb::AABB aabb)
                {
                    math::iv3 min_cell = cell(aabb.min);
                    math::uv3 amt_steps = cell(aabb.max) + 1 - min_cell;

                    u32 amt_cells = amt_steps.x * amt_steps.y * amt_steps.z;
                    
                    if(glm::length(math::v3(amt_steps)) > 10) return {};

                    std::vector<u32> res{};
                    res.reserve(124);

                    for(u32 z = 0; z < amt_steps.z; ++z)
                    {
                        for(u32 y = 0; y < amt_steps.y; ++y)
                        {
                            for(u32 x = 0; x < amt_steps.x; ++x)
                            {
                                math::iv3 c_coords = min_cell + math::iv3(x, y, z);
                                u32 cell = hash(c_coords);
                                res.push_back(cell);
                            }   
                        }    
                    }
                    return res;
                }
            

                void insert(u32 amt_rbs, RigidBody *rbs)
                {
                    for(RigidBody *rb = rbs; rb != rbs + amt_rbs; ++rb)
                    {
                        aabb::AABB aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);
                        //debug::debug.scene->rects.push_back({{1, 0, 0}, aabb.min, aabb.max});
                        std::vector<u32> cells = enumerateCells(aabb);
                        for(u32 c : cells)
                        {
                            cell_start_indices[c]++;
                        }
                    }

                    u32 sum = 0;
                    for(u32 i = 0; i < TABLE_SIZE; ++i)
                    {
                        sum += cell_start_indices[i]; 
                        cell_start_indices[i] = sum;
                    }
                    cell_start_indices[TABLE_SIZE] = sum;

                    for(RigidBody *rb = rbs; rb != rbs + amt_rbs; ++rb)
                    {
                        aabb::AABB aabb = aabb::calcAABB(toM4(rb), rb->ref->vert_positions, rb->ref->amt_verts);
                        std::vector<u32> cells = enumerateCells(aabb);
                        for(u32 c : cells)
                        {
                            cell_start_indices[c]--;
                            cell_entries[cell_start_indices[c]] = rb;
                        }
                    }
                }

                std::unordered_set<RigidBody *> query(aabb::AABB aabb)
                {
                    std::unordered_set<RigidBody *> res{};
                    res.reserve(32);
                    std::vector<u32> cells = enumerateCells(aabb);
                    for(u32 c : cells)
                    {
                        u32 start = cell_start_indices[c];
                        u32 stop = cell_start_indices[c + 1];
                        for(u32 i = start; i < stop; ++i)
                        {
                            res.insert(cell_entries[i]);
                        }
                    }

                    return res;
                }

                void clear()
                {
                    memset(cell_start_indices, 0, TABLE_SIZE * sizeof(u32));
                    memset(cell_entries, 0, MAX_NUM_ENTRIES * sizeof(RigidBody *));
                }
            };

            void init(AABBHashGrid *grid)
            {
                grid->cell_start_indices = mem::MEM<u32>(TABLE_SIZE + 1);
                grid->cell_entries = mem::MEM<RigidBody *>(MAX_NUM_ENTRIES);
                grid->clear();
            }
        }
    }
}