#pragma once

namespace RED
{
    namespace render
    {
        namespace soft_body_shading
        {
            const char *VERT = 
R"glsl(
#version 100
precision mediump float;

attribute vec3 a_pos;
attribute vec3 a_normal;

uniform mat4 projection;
uniform mat4 view;

varying vec3 normal;
varying vec3 pos_in_world;

void main()
{
    gl_Position = projection * view * vec4(a_pos, 1.);

    normal = normalize(a_normal);
    pos_in_world = a_pos; 
}

)glsl";

            const char *FRAG = 

R"glsl(
#version 100
precision mediump float;

varying vec3 normal;
varying vec3 pos_in_world;

vec3 point_light_1_pos = vec3(0., 6., 12.);
vec3 point_light_1_col = vec3(.7);

uniform vec3 color;

vec3 calculate_point_light(vec3 light_pos, vec3 c, float light_bulb_radius)
{
    vec3 dir_to_light = light_pos - pos_in_world;
    float dist_to_light = length(dir_to_light); 
    dir_to_light = normalize(dir_to_light);
    float angle = max(.3, dot(normal, dir_to_light));

    float light_bulb_radius_sq = light_bulb_radius * light_bulb_radius;

    return angle * 60. / (dist_to_light * dist_to_light) * c; 
}

void main()
{
    vec3 final_color = vec3(0.);
    final_color += calculate_point_light(point_light_1_pos, point_light_1_col, .1) * color;    
    gl_FragColor = vec4(vec3(final_color), 1.);
}
)glsl";

        }
    }
}