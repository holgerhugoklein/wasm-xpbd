#pragma once

#include <typedefs.h>

#include "../maths/maths.h"

namespace RED
{
    namespace mem
    {
        template <typename T>
        T* MEM(u32 amt)
        {
            return (T *) malloc(amt * sizeof(T));
        }

        template <typename T>
        T* MEM_Z(u32 amt)
        {
            T *bytes = (T *) malloc(amt * sizeof(T));
            memset(bytes, 0, amt * sizeof(T));
            return bytes;
        }
    }
}