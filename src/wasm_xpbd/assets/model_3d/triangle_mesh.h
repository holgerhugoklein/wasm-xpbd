#pragma once

#include <typedefs.h>

#include "../../maths/maths.h"

namespace RED
{
    namespace assets
    {
        namespace triangle
        {
            struct TriangleMesh
            {
                u32 id;
                u32 amt_verts;
                math::v3 *vert_positions;

                u32 amt_indices;
                u32 *indices;
            };
        }
    }
}