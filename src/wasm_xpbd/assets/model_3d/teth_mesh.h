#pragma once

#include <vector>
#include <algorithm>

#include <typedefs.h>

#include "../../maths/maths.h"


namespace RED
{
    namespace assets
    {
        namespace teth
        {
            struct TethMesh
            {
                u32 id;
                std::vector<math::v3> vertices;
                std::vector<u32> indices_for_rendering;
                std::vector<u32> teth_indices;
                std::vector<u32> teth_edge_indices;
            };

            void scale(TethMesh *mesh, math::m3 scale)
            {
                for(u32 i = 0; i < mesh->vertices.size(); ++i)
                {
                    mesh->vertices[i] = scale * mesh->vertices[i];
                }
            }
            
            void translate(TethMesh *mesh, math::v3 translation)
            {
                for(u32 i = 0; i < mesh->vertices.size(); ++i)
                {
                    mesh->vertices[i] = mesh->vertices[i] + translation;
                }
            }



            struct Edge
            {
                u32 min_id;
                u32 max_id;
                u32 global_edge_number;
            };

            struct EdgeNeighborList
            {
                std::vector<Edge> edges;
            };

            void calculateEdgeNeighborList(std::vector<u32> triangle_indices)
            {
                EdgeNeighborList neighbor_list;
                neighbor_list.edges.reserve(triangle_indices.size());

                for(u32 i = 0; i < triangle_indices.size(); i +=3)
                {
                    u32 i1 = triangle_indices[i];
                    u32 i2 = triangle_indices[i + 1];
                    u32 i3 = triangle_indices[i + 2];

                    u32 global_edge_number_base = 3 * i;

                    Edge e1 = {
                        std::min(i1, i2),
                        std::max(i1, i2),
                        global_edge_number_base
                    };

                    Edge e2 = {
                        std::min(i2, i3),
                        std::max(i2, i3),
                        global_edge_number_base + 1
                    };

                    Edge e3 = {
                        std::min(i3, i1),
                        std::max(i3, i1),
                        global_edge_number_base + 2
                    };

                    neighbor_list.edges.push_back(e1);
                    neighbor_list.edges.push_back(e2);
                    neighbor_list.edges.push_back(e3);
                }

                std::sort(neighbor_list.edges.begin(), neighbor_list.edges.end(), [](Edge lhs, Edge rhs){
                    if(lhs.min_id < rhs.min_id)
                    {
                        return true;
                    }
                    if(lhs.max_id < rhs.max_id)
                    {
                        return true;
                    }
                    if(lhs.global_edge_number < rhs.global_edge_number)
                    {
                        return true;
                    }
                    return false;
                });
            }
        }    
    }
}