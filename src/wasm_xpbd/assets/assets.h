#pragma once

#include "model_3d/teth_mesh.h"

#include "model_3d/bunny.h"

#include "model_3d/rigid_cube.h"
#include "model_3d/rigid_plane.h"

namespace RED
{
    namespace assets
    {
        enum class THETH_MESH_IDS
        {
            BUNNY
        };

        enum class TRI_MESH_IDS
        {
            CUBE,
            PLANE
        };       

        struct Assets
        {
            //teth::TethMesh bunny_teth;

            triangle::TriangleMesh cube_mesh;
            triangle::TriangleMesh plane_mesh;
        };

        void init(Assets *assets)
        {
            //assets.bunny_teth = teth::bunny::loadBunnyTethMesh((u32) THETH_MESH_IDS::BUNNY);
            assets->cube_mesh = triangle::cube::loadCubeTriangleMesh((u32) TRI_MESH_IDS::CUBE);
            assets->plane_mesh = triangle::plane::loadPlaneTriangleMesh((u32) TRI_MESH_IDS::PLANE);
        }    
    }    
}