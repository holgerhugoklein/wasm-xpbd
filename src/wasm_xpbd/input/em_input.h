#pragma once

#include <emscripten/html5.h>

#include <typedefs.h>

#include "./input.h"

#include "../maths/maths.h"

#include "../render/em_opengl.h"

namespace RED
{
    namespace input
    {
        enum class KEYSTATE
        {
            NONE = 0,
            UP,
            DOWN
        };

        enum class MOUSE_KEYS
        {
            LEFT = 0,
            MIDDLE = 1,
            RIGHT = 2,
            AMT
        };

        struct EM_Input
        {
            KEYSTATE keystates[124];
            KEYSTATE prev_keystates[124];
            b32 is_grabbing;
            math::v2 mouseXY{}; 
            math::v2 dmouseXY{}; 
            KEYSTATE mouseStates[(u32) MOUSE_KEYS::AMT]; 
        };

        EM_Input em_input{};

        Input generateFromEM()
        {
            Input input{};

            
            if(em_input.keystates['w'] == KEYSTATE::DOWN)
            {
                input.moveCmds[(u32) MOVE_DIR::FORWARD] = {1.};
            }
            if(em_input.keystates['s'] == KEYSTATE::DOWN)
            {
                input.moveCmds[(u32) MOVE_DIR::BACKWARD] = {1.};
            }
            if(em_input.keystates['a'] == KEYSTATE::DOWN)
            {
                input.moveCmds[(u32) MOVE_DIR::LEFT] = {1.};
            }
            if(em_input.keystates['d'] == KEYSTATE::DOWN)
            {
                input.moveCmds[(u32) MOVE_DIR::RIGHT] = {1.};
            }

            if(em_input.keystates['q'] == KEYSTATE::UP)
            {
                input.step = true;
                em_input.keystates['q'] = KEYSTATE::NONE;
            }
            if(em_input.keystates['g'] == KEYSTATE::UP)
            {
                input.toggle_grav = true;
                em_input.keystates['g'] = KEYSTATE::NONE;
            }
            if(em_input.keystates['e'] == KEYSTATE::UP)
            {
                input.step_once = true;
                em_input.keystates['e'] = KEYSTATE::NONE;
            }
             if(em_input.keystates['r'] == KEYSTATE::UP)
            {
                input.toggle_rot = true;
                em_input.keystates['r'] = KEYSTATE::NONE;
            }

            EmscriptenMouseEvent mouseEvent;
            u32 res = emscripten_get_mouse_status(&mouseEvent);

            math::v2 client_mouseXY = { mouseEvent.clientX, mouseEvent.clientY };
            math::v2 new_mouseXY = render::clientXYToCanvasXY(client_mouseXY);

            em_input.dmouseXY = new_mouseXY - em_input.mouseXY; 
            em_input.mouseXY = new_mouseXY;

            if(em_input.mouseStates[(u32) MOUSE_KEYS::RIGHT] == KEYSTATE::DOWN)
            {
                input.rotCameraCmds = { em_input.dmouseXY };
            }

            input.grab_command.xy_screen = new_mouseXY;
            if(em_input.mouseStates[(u32) MOUSE_KEYS::LEFT] == KEYSTATE::DOWN)
            {
                if(em_input.is_grabbing)
                {
                    input.grab_command.action = GRAB_ACTION::DRAG;
                }
                else
                {
                    input.grab_command.action = GRAB_ACTION::START;
                    em_input.is_grabbing = true;
                }
            }
            else
            {
                if(em_input.is_grabbing)
                {
                    input.grab_command.action = GRAB_ACTION::END;
                    em_input.is_grabbing = false;
                }
                else
                {
                    input.grab_command.action = GRAB_ACTION::NOTHING;
                }
            }

            memcpy(em_input.prev_keystates, em_input.keystates, sizeof(em_input.keystates));

            return input;
        }

        i32 keyUp(i32 evtType, const EmscriptenKeyboardEvent *keyEvent, void *userData)
        {
            if(strlen(keyEvent->key) == 1)
            {
                em_input.keystates[keyEvent->key[0]] = KEYSTATE::UP;
            }
            return false;
        }

        i32 keyDown(i32 evtType, const EmscriptenKeyboardEvent *keyEvent, void *userData)
        {
            if(strlen(keyEvent->key) == 1)
            {
                em_input.keystates[keyEvent->key[0]] = KEYSTATE::DOWN;
            }
            return false;
        }

        i32 mouseUp(i32 evtType, const EmscriptenMouseEvent *mouseEvent, void *userData)
        {  
            em_input.mouseStates[mouseEvent->button] = KEYSTATE::UP;
            return false;
        }

        i32 mouseDown(i32 evtType, const EmscriptenMouseEvent *mouseEvent, void *userData)
        {  
            em_input.mouseStates[mouseEvent->button] = KEYSTATE::DOWN;
            return false;
        }

        i32 mouseMove(i32 evtType, const EmscriptenMouseEvent *mouseEvent, void *userData)
        {  
            return false;
        }

        i32 wheel(i32 evtType, const EmscriptenWheelEvent *wheelEvent, void *userData)
        {
            return false;
        }

        i32 touch(i32 evtType, const EmscriptenTouchEvent *touchEvent, void *userData)
        {
            return false;
        }

        void initInputCallbacks()
        {
            emscripten_set_keyup_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, keyUp);
            emscripten_set_keydown_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, keyDown);

            emscripten_set_mousedown_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, mouseDown);
            emscripten_set_mouseup_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, mouseUp);
            emscripten_set_mousemove_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, mouseMove);

            emscripten_set_wheel_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, wheel);

            emscripten_set_touchstart_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, touch);
            emscripten_set_touchend_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, touch);
            emscripten_set_touchmove_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, &em_input, true, touch);
        }

        void setup()
        {
            initInputCallbacks();   
        }
    }
}