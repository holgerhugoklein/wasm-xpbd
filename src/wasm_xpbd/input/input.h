#pragma once

#include <typedefs.h>

#include "../maths/maths.h"

namespace RED
{
    namespace input
    {
        enum class MOVE_DIR
        {
            FORWARD,
            BACKWARD,
            LEFT,
            RIGHT,
            AMT
        };

        struct MoveCommand
        {
            f32 amt = 0.;
        };

        struct RotateCameraCommand
        {
            math::v2 xy;
        };

        enum class GRAB_ACTION
        {
            NOTHING,
            START,
            END,
            DRAG
        };

        struct GrabCommand
        {
            GRAB_ACTION action;
            math::v2 xy_screen;
        };

        struct Input
        {
            MoveCommand moveCmds[(u32) MOVE_DIR::AMT];
            RotateCameraCommand rotCameraCmds;

            GrabCommand grab_command;

            b32 stop_running;
            b32 step;
            b32 step_once;
            b32 toggle_grav;
            b32 toggle_rot;
        };

        math::v4 getMoveVec(Input input)
        {
            return 
            {
                input.moveCmds[(u32) MOVE_DIR::FORWARD].amt, 
                input.moveCmds[(u32) MOVE_DIR::BACKWARD].amt, 
                input.moveCmds[(u32) MOVE_DIR::LEFT].amt, 
                input.moveCmds[(u32) MOVE_DIR::RIGHT].amt
            };
        }
    }
}